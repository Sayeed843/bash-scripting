#! /bin/bash

while read line
do
	echo "$line"
done < "${1:-/dev/stdin}"

: '
here, $1 is the given file input and 
/dev/stdin is the path of stdin. We have tried 
to read file through stdin. '
