#! /bin/bash

echo "Enter a string"
read st


echo "First letter will be Upper: ${st^}"   # ^ sign represents the Uppercase
echo "All letters will be Upper: ${st^^}"  # ^^ sign represents the lowercase
