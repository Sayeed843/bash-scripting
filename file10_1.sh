#! /bin/bash

echo "Enter the first number:" 
read n1

echo "Enter the second number:"
read n2

echo "Addition = $(( n1 + n2 ))"
echo "Subtraction = $(( n1 - n2 ))"
echo "Multiply = $(( n1 * n2 ))"
echo "Division = $(( n1 / n2 ))"
echo "Reminder = $(( n1 % n2 ))"
