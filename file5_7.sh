#! /bin/bash

for (( num=0; num<=10; num++ ))
do 
	if [[ "$num" -eq 3 ]] || [[ "$num" -eq 7 ]]
	then 
		continue
	fi
	echo $num
done
