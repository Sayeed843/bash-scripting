#! /bin/bash

number=1

:'
Your system will not execute until-loop, 
when its condition is True. 
'
until [ $number -ge 10 ]
do 
	echo $number
	number=$(( number+1 ))
done
