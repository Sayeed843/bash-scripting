#! /bin/bash

MESSAGE="Hello Bash Script Learner!"

# Now we will send MESSAGE variable data to the another script
# That's why, we should export this variable and add 2nd script
# file path. 

export MESSAGE
./file8_2.sh
