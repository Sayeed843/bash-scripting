#! /bin/bash

grade=73
if (( "$grade" >= 80 ))
then 
	echo "Result=A+"
elif [ "$grade" -lt 79  -a  "$grade" -ge 75 ]
then
	echo "Result=A"
elif [ "$grade" -ge 70 -a "$grade" -lt 74 ]
then 
	echo "Result=A-"
elif [ "$grade"  -ge 60 -a "$grade" -lt 69 ]
then 
	echo "Result=B"
else
	echo "Result=F"
fi  
